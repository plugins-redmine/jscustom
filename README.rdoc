= Description
Include file js in layout general of redmine, js and css work by default addsubtask in popup and return issue parent
- Author_url : 'http://orotorres.com'
- Version : 0.0.1
- Compatibility Redmine : 2.x, 3.x
= Install
- Locate in plugins/
- git clone git@gitlab.com:plugins-redmine/jscustom.git
- reiniciar redmine
= Locate file js and css
- plugins/jscustom/assets/javascript/jscustom.js
- plugins/jscustom/assets/stylesheets/jscustom.css