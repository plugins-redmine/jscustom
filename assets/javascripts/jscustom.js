$(document).ready(function() {
	$("body.controller-issues #issue_tree .contextual a").addClass("fancypopup");
    var w = 912, h = 520;
	$(".fancypopup").click(function(){
		window.open($(this).attr('href')+'#rogged-parentissue','new','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width='+w+',height='+h+', top=100, left=100');
		return false;
	});
	var xurl_string = window.location.href;
	xurlarray = xurl_string.split('#')
	if(xurlarray.includes('rogged-parentissue')){
		xfrmaction=$('#issue-form').attr('action');
		$('#issue-form').attr('action',xfrmaction+'#rogged-parentissue');
		$('body.controller-issues #wrapper3 #top-menu').remove();
		$('body.controller-issues #wrapper3 #header').remove();

		$('input[name="commit"]').remove();
		if($('#flash_notice').length > 0){
			window.opener.location.reload();
			window.close();
		}
	}
});