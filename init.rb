require_dependency 'jscustom_hook_listener'

Redmine::Plugin.register :jscustom do
  name 'Redmine Custom JS plugin'
  author 'Oscar Torres'
  description 'Include file js in layout general of redmine, js default addsubtask in popup and return issue parent'
  version '0.0.1'
  url 'http://orotorres.com'
  author_url 'http://orotorres.com'
end
