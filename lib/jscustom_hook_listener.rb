class JscustomHookListener < Redmine::Hook::ViewListener
	def view_layouts_base_html_head(context)
			'<link rel="stylesheet" type="text/css" href="/plugin_assets/jscustom/stylesheets/jscustom.css?v=2" media="screen" /><script type="text/javascript" src="/plugin_assets/jscustom/javascripts/jscustom.js?v=2"></script>'
	end
end
